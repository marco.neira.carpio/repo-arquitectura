# Repo-arquitectura



## Wiki

Ir al Wiki de los [Documentos de Arquitectura](https://gitlab.com/marco.neira.carpio/repo-arquitectura/-/wikis/home)


## Archivos

[Lista de archivos](https://gitlab.com/marco.neira.carpio/repo-arquitectura/-/tree/main/archivos?ref_type=heads)

Ir al archivo del [Diccionario de Arquitectura](https://gitlab.com/marco.neira.carpio/repo-arquitectura/-/blob/main/archivos/Diccionario.md?ref_type=heads)
