# Diccionario de Arquitectura

## Base de Datos

> **_Contexto:_** Sistemas  
> **_Definición:_** Una base de datos es una colección estructurada de información o datos que se almacena de manera organizada, con el propósito de permitir la gestión, recuperación y manipulación eficiente de esos datos. En términos más simples, es un repositorio electrónico donde se almacenan datos de manera que puedan ser fácilmente accedidos, gestionados y actualizados.  
> **_Ultima Actualización:_** 20/10/2023  
> **Referencias Externas:**  
> **Referencias Internas:**

---

## Arquitectura de Software

> **_Contexto:_** Desarrollo de Software  
> **_Definición:_** Es la estructura fundamental y el diseño general de un sistema de software. Es el marco que define cómo se organizan y relacionan los componentes de un software y cómo interactúan entre sí para lograr los objetivos de la aplicación.  
> **_Ultima Actualización:_** 20/10/2023  
> **Referencias Externas:**  
> **Referencias Internas:**

---

## Identity Provider

> **_Contexto:_** Keycloak  
> **_Definición:_** Es un proveedor de identidad, un componente que se utiliza para autenticar a los usuarios y proporcionar información sobre sus identidades. Un Identity Provider (Proveedor de Identidad) puede ser una API con reglas de identidad personalizadas que Keycloak puede utilizar para autenticar a los usuarios.  
> **_Ultima Actualización:_** 20/10/2023  
> **Referencias Externas:** [https://medium.com/keycloak/keycloak-as-an-identity-broker-an-identity-provider-af1b150ea94](https://medium.com/keycloak/keycloak-as-an-identity-broker-an-identity-provider-af1b150ea94)  
> **Referencias Internas:**

---

## Interfaz

> **_Contexto:_** Sistemas  
> **_Definición:_** Se refiere a cualquier punto de interacción entre un usuario y una máquina o un sistema.  
> **_Ultima Actualización:_** 20/10/2023  
> **Referencias Externas:**  
> **Referencias Internas:**

---

## Interfaz

> **_Contexto:_** Desarrollo de Software  
> **_Definición:_** Conjunto de reglas o protocolos que permiten que diferentes aplicaciones se comuniquen entre sí.  
> **_Ultima Actualización:_** 20/10/2023  
> **Referencias Externas:**  
> **Referencias Internas:**

---

## Identidad Digital

> **_Contexto:_** Normativo  
> **_Definición:_** Es el conjunto de atributos (inherentes y complementarios) que permite reconocer a una persona en el entorno digital distinguiendolos de otros. Los atributos inherentes son : los nombres y apellidos, fecha de nacimiento, número de teléfono celular, etc; mientras que los complementarios, de la mano con los anteriores, permiten identificar a una persona desde un ámbito social, económico o judicial, como su profesión, calificación como contribuyente, etc. 
De esta manera, se reconoce a la persona, sin que esté presente físicamente, a través de plataformas y servicios digitales seguros, accesibles y confiables. Con la identidad digital, se pueden realizar acciones de comercio y gobierno digital, entre otras.  
> **_Ultima Actualización:_** 20/10/2023    
> **Referencias Externas:** https://guias.servicios.gob.pe/creacion-servicios-digitales/gobierno-digital/identidad  
> **Referencias Internas:**

## Arquitectura Digital

> **_Contexto:_** Definición Conceptual  
> **_Definición:_** Conjunto de componentes, lineamientos y estándares, que permiten alinear los sistemas de información, datos, seguridad e infraestructura tecnológica.  
> **_Ultima Actualización:_** 20/10/2023  
> **Referencias Externas:** https://www.gob.pe/institucion/pcm/tema/arquitectura-digital  
> **Referencias Internas:**
