-- pantalla 1

SELECT sec_ejec, nombre_entidad, usuario, numero_documento_concat,
nombre || ' ' || apellido_paterno || ' ' || apellido_materno AS nombre_completo, area, TO_CHAR(fecha_creacion,'DD/MM/YYYY HH24:MI:SS') AS fecha_creacion, ultima_actividad,
creado_por, descripcion_estado, estado, tipo_usuario, tipo_usuario_aplicacion
FROM seguridad.usuario_detalle_siafadm_vw
WHERE sec_ejec = 1027--:secEjec
--AND usuario = :usuario
--AND numero_documento = '08785153' --:numeroDocumento
AND estado = 'A' --:estado


-- pantalla 3
--Pantalla 3:
SELECT a.rol, a.descripcion, a.nombre  , NVL(urd.estado,'I') estado_usuario_rol         
FROM seguridad.rol a,        
    (SELECT ur.rol, ur.estado         
        FROM seguridad.usuario_rol ur           
        WHERE usuario     = :usuario      
        AND ur.estado   = 'A'
        AND EXISTS (SELECT 1 FROM seguridad.rol r           
                    WHERE ur.rol = r.rol         
                    AND r.rol_padre = :rolPadre)) urd
WHERE a.rol_padre   = :rolPadre                          
AND a.estado      = 'A'
AND a.rol         = urd.rol (+)         
AND DECODE(NVL(a.control_visibilidad,'N'),'S',seguridad_web.evalua_control_rol(a.rol, :usuario , :secEjec ),'OK') = 'OK'

--rolpadre 399
--usuario RU13776
-- secejec 1027

--Pantalla 4:
-------------
SELECT c.etiqueta,
c.codigo, a.aplicacion,  c.aplicacion_padre, LEVEL , a.rol ,
seguridad.seguridad_web.dev_privilegio_x_usuario_aplic(:usuario, a.aplicacion, 2) privilegio_consulta,
seguridad.seguridad_web.dev_tiene_hijo_configurable(a.aplicacion, :usuario) privilegio_operacion
FROM seguridad.rol_aplicacion_privilegio a, 
seguridad.aplicacion c  
WHERE a.aplicacion  = c.aplicacion  
 AND a.rol         = :rol     
AND c.tipo_opcion IN (1,2,7)  
AND c.estado = 'A'  
/*if (esAdministrador != null) {
    AND NVL(c.es_opcion_admin,'N') = DECODE (:esAdmin, 'N', 'N',  NVL(c.es_opcion_admin,'N'))
}
if (indicadorEntidad != null) {
    if (indicadorEntidad > 1) {
        indicadorEntidad = 2;
    }
    AND (INSTR(c.es_opcion_entidad,:indicadorEntidad) > 0 OR c.es_opcion_entidad IS NULL)
}
if (esOpcionEspecifica != null) {
    AND (INSTR(:opcionEspecifica,c.es_opcion_especifica) > 0 OR c.es_opcion_especifica IS NULL)
}*/
AND DECODE(NVL(c.control_visibilidad,'N'),'S',seguridad_web.evalua_control_aplicacion(c.aplicacion,:usuario,:secEjec),'OK') = 'OK' 
START WITH c.aplicacion in (SELECT aplicacion 
                           FROM seguridad.aplicacion a
                           WHERE aplicacion_padre = :aplicacion 
                           AND EXISTS (SELECT 1 FROM seguridad.rol_aplicacion_privilegio b 
                                     WHERE a.aplicacion = b.aplicacion 
                                     AND b.rol = :rol ) ) 
CONNECT BY PRIOR c.aplicacion = c.aplicacion_padre     
ORDER SIBLINGS BY c.orden, c.aplicacion

-- para operaciones en linea
----------------------------
-- rol 192
-- aplicacion 12891
-- usuario RU13776
-- secejec 1027

-- para plataforma siaf
-----------------------
-- aplicacion 20002
--RU12146
--399
--1027

 
-- Vista de pantalla 4
----------------------
-- roles Zully
SELECT b.nombre_rol, a.*  
  FROM ( SELECT lpad(' ', (level - 1) * 2/*, '+'*/) || c.etiqueta nombre_opcion, 
                c.codigo, c.aplicacion,  c.aplicacion_padre, LEVEL, c.tipo_opcion, c.es_configurable, comando, estado, 
                ROWNUM orden_anidado 
           FROM seguridad.aplicacion c 
         START WITH aplicacion = (SELECT aplicacion 
                                    FROM seguridad.aplicacion 
                                   WHERE codigo = 'SIAFADM' )
         CONNECT BY PRIOR aplicacion = aplicacion_padre
         ORDER SIBLINGS BY c.orden   ) a, 
       ( SELECT a.aplicacion, b.nombre nombre_rol 
           FROM seguridad.rol_aplicacion_privilegio a, 
                seguridad.rol b
          WHERE a.rol = b.rol  ) b
 WHERE a.aplicacion = b.aplicacion  (+)
   AND b.nombre_rol = 'Administrativo'
 ORDER BY orden_anidado;  
 
 
--- puras pruebas
-----------------

Select w.*
From (
SELECT a.rol, b.codigo, b.aplicacion, b.etiqueta, a.descripcion nombre_rol,
DECODE(NVL(d.estado,'I'),'I',NULL,DECODE(d.usuario_modificacion,NULL,d.usuario_creacion,d.usuario_modificacion)) usuario_creacion,
DECODE(NVL(d.estado,'I'),'I',NULL,DECODE(d.fecha_modificacion,NULL,to_char(d.fecha_creacion, 'DD/MM/YYYY HH24:MI:SS'),to_char(d.fecha_modificacion, 'DD/MM/YYYY HH24:MI:SS')  ) ) fecha_creacion,
e.descripcion estado, NVL(d.estado,'I') codigo_estado, NVL(a.es_configurable, 'N') configurable, a.es_rol_padre, CAST('N' AS VARCHAR(2)) As es_dggfrh, a.nombre
FROM seguridad.rol a,
seguridad.aplicacion b,
seguridad.rol_aplicacion c,
(SELECT ur.rol, ur.estado, ur.fecha_creacion, ur.usuario_creacion, ur.fecha_modificacion, ur.usuario_modificacion, 'N' As es_dggfrh
FROM seguridad.usuario_rol ur
WHERE ur.usuario = :usuario ) d,
siafmef.codigo_general_det e 
WHERE a.es_rol_x_aplicacion IN ('S','T')
AND decode(a.es_rol_x_aplicacion,'T',8,0)= decode(a.es_rol_x_aplicacion,'T',b.tipo_opcion,0)
AND a.rol = c.rol
AND b.aplicacion = c.aplicacion
AND a.rol = d.rol (+)
AND NVL(d.estado,'I') = e.codigo_det
AND e.codigo = 'ESTADO'
AND NVL(a.es_rol_interno,'N') = 'N'
if (indicadorEntidad == 0) {
    AND (A.ES_ROL_ENTIDAD = '0'
} else if (indicadorEntidad == 1) {
    AND (A.ES_ROL_ENTIDAD = '1'
} else {
    AND (A.ES_ROL_ENTIDAD IN ('0','2')
}
OR  A.ES_ROL_ENTIDAD IS NULL)
AND a.estado = 'A'
AND DECODE(NVL(a.control_visibilidad,'N'),'S',seguridad_web.evalua_control_rol(a.rol,:usuario,:secEjec),'OK') = 'OK'
if (ejecutora.getTipoUnidad().equals(M)) {
    AND DECODE(b.codigo, 'SISMEPRE', SUBSTR(a.descripcion, 6, 2), 1) =
    DECODE(b.codigo, 'SISMEPRE', (SELECT DISTINCT(ee.tipo_meta) FROM rentas.dgpp_entidad_estado ee
    WHERE ee.ano_aplicacion = 2023--:anoEje
    AND ee.sec_ejec = 1027 ), 1)
}    
Union
Select                                                                                                                                                                                                   
x.rol, x.codigo, x.aplicacion, x.etiqueta, x.nombre_rol, Min(x.usuario_creacion) usuario_cracion, Min(x.fecha_creacion) fecha_creacion,                                                   
x.estado, x.codigo_estado, x.configurable, x.es_rol_padre, CAST('S' AS VARCHAR(2)) As es_dggfrh , x.nombre                                                                                                                      
From                                                                                                                                                                                                      
(                                                                                                                                                                                                         
Select                                                                                                                                                                                        
0 as rol, b.codigo, b.aplicacion, b.etiqueta, b.codigo As nombre_rol,                                                                                                                 
DECODE(NVL(d.estado,'I'),'I',NULL,DECODE(d.usuario_modificacion,NULL,d.usuario_creacion,d.usuario_modificacion)) usuario_creacion,                                                               
DECODE(NVL(d.estado,'I'),'I',NULL,DECODE(d.fecha_modificacion,NULL,to_char(d.fecha_creacion, 'DD/MM/YYYY HH24:MI:SS'),to_char(d.fecha_modificacion, 'DD/MM/YYYY HH24:MI:SS')  ) ) fecha_creacion,
e.descripcion estado, NVL(d.estado,'I') codigo_estado, NVL(a.es_configurable, 'N') configurable, a.es_rol_padre , b.codigo nombre                                                                                 
From seguridad.rol a                                                                                                                                                                                  
Inner Join seguridad.rol_aplicacion     c On a.rol = c.rol And c.estado = 'A'                                                                                                                        
Inner Join seguridad.aplicacion         b On b.aplicacion = c.aplicacion And b.estado = 'A'                                                                                                          
Inner Join seguridad.usuario_rol        d On a.rol = d.rol And d.estado = 'A'                                                                                                                        
Inner Join siafmef.codigo_general_det   e On NVL(d.estado,'I') = e.codigo_det And e.codigo = 'ESTADO'                                                                                                 
Where a.tipo_rol = 7                                                                                                                                                                                 
And d.usuario = 'RU13776' --:usuario                                                                                                                                                                            
) x                                                                                                                                                                                                      
Group by x.rol, x.codigo, x.aplicacion, x.etiqueta, x.nombre_rol, x.estado, x.codigo_estado, x.configurable, x.es_rol_padre , x.nombre                                                                       
) w
        
ORDER BY w.etiqueta, w.nombre
 
 
 -------------------------------------
 
 
select * from seguridad.rol
where nombre like '%Plataforma SIAF%'

select * from seguridad.rol
where rol_padre=399

select * from seguridad.rol
where DESCRIPCION like '%ACCESO AL MODULO TICKET SOPORTE%'

SELECT * FROM SEGURIDAD.USUARIO_ROL WHERE ROL in (584,401,403,503,506) 
and estado = 'A'
and usuario = 'RU13776'

--------------------------------
select * from SEGURIDAD.rol_aplicacion_privilegio where rol = 192
select * from seguridad.aplicacion where aplicacion = 12892
select * from seguridad.aplicacion where aplicacion_padre = 12891
 
 
---------------------------------------------------------------------
-- arbol antiguo
-------------------
SELECT aplicacion, aplicacion_padre, orden,
       codigo, SYS_CONNECT_BY_PATH(codigo,'  >') jerarquia_codigo, 
       SYS_CONNECT_BY_PATH(aplicacion,'  >') jerarquia_aplicacion, etiqueta, titulo, level
  FROM seguridad.aplicacion
 START WITH aplicacion = 20002
CONNECT BY PRIOR aplicacion = aplicacion_padre
 ORDER siblings BY orden

seguridad.rol_aplicacion_privilegio
seguridad.aplicacion


select * from seguridad.rol_aplicacion_privilegio 
where rol = 192



---------------------
-- solucion
---------------------
-- depurar alguna data
delete seguridad.aplicacion where aplicacion = 20006;
delete seguridad.aplicacion where aplicacion = 19984;


BEGIN
	-- Llena plantilla
    -- inserto en aplicacion_privilerio, cartesiano completo nivel 2 y 3
    FOR i IN ( select distinct  aplicacion from (
                SELECT aplicacion, aplicacion_padre, orden,
                       codigo, SYS_CONNECT_BY_PATH(codigo,'  >') jerarquia_codigo, 
                       SYS_CONNECT_BY_PATH(aplicacion,'  >') jerarquia_aplicacion, 
					   etiqueta, titulo, level
                  FROM seguridad.aplicacion
                 WHERE level in (2,3)
                 START WITH aplicacion = 20002
                CONNECT BY PRIOR aplicacion = aplicacion_padre
                 ORDER siblings BY orden) )
    LOOP
        insert into seguridad.aplicacion_privilegio (aplicacion, privilegio)
        values (i.aplicacion,1);
        insert into seguridad.aplicacion_privilegio (aplicacion, privilegio)
        values (i.aplicacion,2);
    END LOOP;
    -- personalizar los privilegios
		-- ej. 1. Quitar de la vista el menu Registro (20003) y privilegio operacion (2)
    DELETE seguridad.aplicacion_privilegio WHERE aplicacion = 20003;
	DELETE seguridad.aplicacion_privilegio WHERE aplicacion = 19987;
	DELETE seguridad.aplicacion_privilegio WHERE aplicacion = 19986;
	DELETE seguridad.aplicacion_privilegio WHERE aplicacion = 19985;
	DELETE seguridad.aplicacion_privilegio WHERE aplicacion = 22329 AND privilegio=2;
	DELETE seguridad.aplicacion_privilegio WHERE aplicacion = 19925 AND privilegio=2;
	DELETE seguridad.aplicacion_privilegio WHERE aplicacion = 19921 AND privilegio=2;
	DELETE seguridad.aplicacion_privilegio WHERE aplicacion = 19966 AND privilegio=2;
	DELETE seguridad.aplicacion_privilegio WHERE aplicacion = 23033 AND privilegio=2;
	-- fin llena plantilla
	
END;


--delete seguridad.aplicacion_privilegio where usuario_creacion = 'MNEIRA' and fecha_creacion > sysdate -2
select * from seguridad.aplicacion_privilegio where usuario_creacion = 'MNEIRA' and fecha_creacion > sysdate -2
--delete seguridad.usuario_aplicacion_privilegio where usuario_creacion = 'MNEIRA' and fecha_creacion > sysdate -2
select * from seguridad.usuario_aplicacion_privilegio where usuario_creacion = 'MNEIRA' and fecha_creacion > sysdate -2
select * from seguridad.aplicacion_privilegio where aplicacion = 20002


-- inserto relacion entre el rol y el modulo (el que no es convivencia)
-- delete seguridad.rol_aplicacion where aplicacion =20002 and rol=399; 
insert into seguridad.rol_aplicacion (rol, aplicacion, estado) values (399,20002,'A');


------------------------------------
-- carga rol_aplicacion_privilegio
------------------------------------

BEGIN
    FOR i IN ( select distinct  aplicacion from (
                SELECT aplicacion, aplicacion_padre, orden,
                       codigo, SYS_CONNECT_BY_PATH(codigo,'  >') jerarquia_codigo, 
                       SYS_CONNECT_BY_PATH(aplicacion,'  >') jerarquia_aplicacion, 
					   etiqueta, titulo, level
                  FROM seguridad.aplicacion
                 WHERE level in (2,3)
                 START WITH aplicacion = 20002
                CONNECT BY PRIOR aplicacion = aplicacion_padre
                 ORDER siblings BY orden) )
    LOOP
        
        insert into seguridad.rol_aplicacion_privilegio (rol, aplicacion, privilegio, estado)
        values (399,i.aplicacion,2,'A');
        update seguridad.aplicacion set es_configurable ='S' where aplicacion_padre = i.aplicacion and tipo_opcion=3;
    END LOOP;
END;

-----------------------------------
-- ejecutar archivo nuevos botones
-- despues continuar 
-----------------------------------

-------------------------------
-- vista de soluction alterna
-----------------------------
SELECT c.etiqueta,
c.codigo, a.aplicacion,  c.aplicacion_padre, LEVEL , a.rol ,
seguridad.seguridad_web.dev_privilegio_x_usuario_aplic('08785153', a.aplicacion, 2) privilegio_consulta,
seguridad.seguridad_web.dev_tiene_hijo_configurable(a.aplicacion, '08785153') privilegio_operacion
FROM seguridad.rol_aplicacion_privilegio a, 
seguridad.aplicacion c  
WHERE a.aplicacion  = c.aplicacion  
 AND a.rol         = 399    
 AND ( c.tipo_opcion IN (1,2)  OR (c.tipo_opcion = 7 AND EXISTS (SELECT 1 FROM seguridad.rol f WHERE f.rol = a.rol AND tipo_rol = 8)) )
AND c.estado = 'A'  
/*if (esAdministrador != null) {
    AND NVL(c.es_opcion_admin,'N') = DECODE (:esAdmin, 'N', 'N',  NVL(c.es_opcion_admin,'N'))
}
if (indicadorEntidad != null) {
    if (indicadorEntidad > 1) {
        indicadorEntidad = 2;
    }
    AND (INSTR(c.es_opcion_entidad,:indicadorEntidad) > 0 OR c.es_opcion_entidad IS NULL)
}
if (esOpcionEspecifica != null) {
    AND (INSTR(:opcionEspecifica,c.es_opcion_especifica) > 0 OR c.es_opcion_especifica IS NULL)
}*/
AND DECODE(NVL(c.control_visibilidad,'N'),'S',seguridad_web.evalua_control_aplicacion(c.aplicacion,'08785153',1027),'OK') = 'OK' 
START WITH c.aplicacion in (SELECT aplicacion 
                           FROM seguridad.aplicacion a
                           WHERE aplicacion_padre = 20002--:aplicacion 
                           AND EXISTS (SELECT 1 FROM seguridad.rol_aplicacion_privilegio b 
                                     WHERE a.aplicacion = b.aplicacion 
                                     AND b.rol = 399 ) ) 
CONNECT BY PRIOR c.aplicacion = c.aplicacion_padre     
ORDER SIBLINGS BY c.orden, c.aplicacion


-- para plataforma siaf
-----------------------
-- aplicacion 20002
--RU12146
--399
--1027

-- para plataforma siaf
-----------------------
--aplicacion 20002
--USUARIO '08785153'
--ROL 399
--SEC_EJEC 1027


-------------- borrar la data------------

select * from seguridad.usuario_aplicacion_privilegio
where aplicacion in (
                            SELECT aplicacion FROM (
                            SELECT aplicacion, aplicacion_padre, orden,
                                   codigo, SYS_CONNECT_BY_PATH(codigo,'  >') jerarquia_codigo, 
                                   SYS_CONNECT_BY_PATH(aplicacion,'  >') jerarquia_aplicacion, 
                                   etiqueta, titulo, level
                              FROM seguridad.aplicacion
                             START WITH aplicacion = 20002
                            CONNECT BY PRIOR aplicacion = aplicacion_padre
                             ORDER siblings BY orden)
                            )
                            
delete seguridad.usuario_aplicacion_privilegio
where aplicacion in (
                            SELECT aplicacion FROM (
                            SELECT aplicacion, aplicacion_padre, orden,
                                   codigo, SYS_CONNECT_BY_PATH(codigo,'  >') jerarquia_codigo, 
                                   SYS_CONNECT_BY_PATH(aplicacion,'  >') jerarquia_aplicacion, 
                                   etiqueta, titulo, level
                              FROM seguridad.aplicacion
                             START WITH aplicacion = 20002
                            CONNECT BY PRIOR aplicacion = aplicacion_padre
                             ORDER siblings BY orden)
                            )
							